<?php

require_once("Animal.php");
require_once("Ape.php");
require_once("Frog.php");


$sheep = new Animal("shaun");

echo "Nama : $sheep->name <br>"; // "shaun"
echo "Legs : $sheep->legs <br>"; // 4
echo "Cold Blooded: $sheep->cold_blooded <br>"; // "no"
echo "<br>";

$frog = new Frog("buduk");

echo "Nama : $frog->name <br>"; // "shaun"
echo "Legs : $frog->legs <br>"; // 4
echo "Cold Blooded: $frog->cold_blooded <br>"; // "no"
// echo "Cold : $frog->jump() <br>"; // "no"
echo  $frog->jump() . "<br>";
echo "<br>";

$ape = new Ape("kera sakti");

echo "Nama : $ape->name <br>"; // "shaun"
echo "Legs : $ape->legs <br>"; // 4
echo "Cold Blooded: $ape->cold_blooded <br>"; // "no"
// echo "Cold : $ape->yell() <br>"; // "no"
echo  $ape->yell() . "<br>";
echo "<br>";